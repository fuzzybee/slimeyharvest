var gameData = {
    slimes: 0,
    helpers: 0,
    helpersCost: 10
};

function slimesClick(number) {
    gameData.slimes = gameData.slimes + number;
    document.getElementById('slimes').innerHTML = gameData.slimes;
};

function buyHelpers() {
    var helpersCurrentCost = Math.floor(gameData.helpersCost * Math.pow(1.1, gameData.helpers));
    if (gameData.slimes >= helpersCurrentCost) {
        gameData.helpers = gameData.helpers + 1;
        gameData.slimes = gameData.slimes - helpersCurrentCost;
        document.getElementById('helpers').innerHTML = gameData.helpers;
        document.getElementById('slimes').innerHTML = gameData.slimes;
    };
    var helpersCost = Math.floor(gameData.helpersCost * Math.pow(1.1, gameData.helpers));
    document.getElementById('helpersCost').innerHTML = helpersCost;
    gameData.helpersCost = helpersCost
};

function saveGame() {
    window.localStorage.setItem('gameData', JSON.stringify(gameData));
};

function loadGame() {
    if (window.localStorage.getItem('gameData')) {
        gameData = JSON.parse(window.localStorage.getItem('gameData'));
        updateAll();
    }
};

function updateAll() {
    document.getElementById('helpers').innerHTML = gameData.helpers;
    document.getElementById('slimes').innerHTML = gameData.slimes;
    document.getElementById('helpersCost').innerHTML = gameData.helpersCost;
}

window.setInterval(function() {
    slimesClick(gameData.helpers);
}, 1000);